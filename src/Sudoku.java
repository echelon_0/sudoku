import java.util.*;


public class Sudoku {
	
	Random md = new Random();
	int [] numbers = new int[9];
	int [][] sudoku99 = new int[9][9];;
	int count = 1, random;
	int t;
	ArrayList<Integer> list = new ArrayList<Integer>();
	boolean num = false;
				
	void startSudoku() {
			createSudoku();
	}

	void createSudoku() {

			for( int k = 0; k < 9; k++ ) {
					for( int i = 0; i < 9; i++ ) {
							System.out.print("|");
							
							if( i != 0 && i % 3 == 0 ) {
									System.out.print(" |");
							}

							num = true;
							while( num ) {
								// Random Number
								numbers[i] = md.nextInt(9) +1;

								// Control
								if( horizontalControl( k, numbers[i] ) && verticalControl( i, numbers[i] ) ) {
										sudoku99[k][i] = numbers[i];
										t = numbers[i];
										num = false;
								}
								else {
										t = 0;
										num = false;
								}
							}
							
							// Add Numbers or * 
							if( t != 0)
								System.out.print( t );
							else
								System.out.print("*");

					}

					System.out.println("|");

					if( (k + 1) % 3 == 0 ) {
						System.out.println(" ");
					}

			}	

	}


	boolean horizontalControl( int horizontal, int number ) {
		for( int i = 0; i < 9; i++ ) {
				if( sudoku99[horizontal][i] == number )
							return false;
			}
		return true;
	}

	boolean verticalControl( int vertical, int number ) {
		for( int i = 0; i < 9; i++ ) {
				if( sudoku99[i][vertical] == number )
						return false;
			}
		return true;
	}
		
		
}
