import java.util.Random;


class BasicSudoku {

				Random rnd = new Random();
				int sudoku[][] = new int[9][9];
				int numbers[] = new int[9];
				int count = 0;
				int copy[] = new int[9];
				boolean sx = true;
				
				public void startSudoku() {
								createSudoku();
				}
				
				public void createSudoku() {
								for (int j = 0; j < 9; j++) {
												for (int i = 0; i < 9; i++) {
																numbers[i] = rnd.nextInt(9) +1;
															
																if( horizontalControl(j, numbers[i]) && verticalControl(i, numbers[i]) )
																				sudoku[j][i] = numbers[i];
																else {
																				count++;
																				
																				for (int x = 0; x < 9; x++) {
																								sudoku[j][x] = 0;
																								numbers[x] = 0;
																				}
																																							
																				i = -1;
																				
																				// if( j == 8 && count == 81 ) {
																				// 				j = ;
																				// 				count = 0;
																				// }
																				
																}

																																																				
												} // For i

												// Reset numbers[]
												for (int x = 0; x < 9; x++) {
																numbers[x] = 0;															
												}
												// if( j == 7 )
												// 				printSudoku();

								} // For j
								printSudoku();
				}

				public boolean horizontalControl( int horizontal, int number ) {
								for (int i = 0; i < 9; i++) {
												if( sudoku[horizontal][i] == number )
																return false;
								}

								return true;
				}

				public boolean verticalControl( int vertical, int number ) {
								for (int i = 0; i < 9; i++) {
												if( sudoku[i][vertical] == number )
																return false;
								}

								return true;
				}

				public void printSudoku() {
								System.out.println("-----------------------");

								for (int j = 0; j < 9; j++) {
												System.out.print("|");
												
												for (int i = 0; i < 9; i++) {
																if( i % 3 == 0 && i != 0)
																				System.out.print(" |");
																if( sudoku[j][i] != 0 )
																				System.out.print(sudoku[j][i] + "|");
																else
																				System.out.print("-8-" + sudoku[j][i]);
												}

												if( ( j + 1 ) % 3 == 0 ) {
																System.out.println();
																System.out.print("-----------------------");
												}
												System.out.println();
								}
				}

				

				
}
